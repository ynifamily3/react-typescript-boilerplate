import adder from "./adder";

export default function testAdd() {
  const result = adder(1, 2);
  if (result === 3) {
    console.info("맞아");
  } else {
    console.error("안돼!");
    throw new Error("안돼!-에러.");
  }
}

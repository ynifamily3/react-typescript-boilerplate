# 리액트 프로젝트 제로베이스로 설계하기

- Typescript
- React.js
- Webpack
- eslint
- prettier

를 제로 베이스로 세팅해 봅니다.

실행법

- dev인 경우엔 yarn dev로 실행하여 http://localhost:3000 확인
- production인 경우엔 yarn build 후 yarn start로 실행하여 http://localhost:3000 확인

# 깃랩으로 옮겼어요

- 안녕하세요

# 이것을 잘 사용하려면?

- webpack.config.ts를 참고해 보세요.

## 기능1

- xxx를 한다.
- yyy를 한다.
